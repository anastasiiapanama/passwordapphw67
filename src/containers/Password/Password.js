import React, {useState} from 'react';
import './Password.css';
import {useDispatch, useSelector} from "react-redux";

const Password = () => {
    const [pass, setPass] = useState({
        password: ''
    });

    const [isDisabled, setIsDisabled] = useState(false);

    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const buttons = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0'];

    const changeNum = (e) => {
        setPass({...pass, password: pass.password += e.target.value});
        if (pass.password.length === 4) {
            setIsDisabled(true);
        }
    };

    const checkPass = () => {
        dispatch({type: 'ADD_NUM', value: pass.password});
    };

    const deleteNumber = () => {
        const copyPass = {...pass};
        const newStr = copyPass.password.slice(0, -1);
        console.log(newStr);
        setPass({password: newStr});
    };

    let numberClasses = ["Numbers"];

    if (state.access === true) {
        numberClasses.push("Green");
    } else if (state.access === false) {
        numberClasses.push("Red");
    }

    return (
        <div className="Password-block">
            <div className="Form">
                <div className="Password">
                    <h1 className={numberClasses.join(" ")}>{pass.password}</h1>
                </div>
                <div className="Buttons">
                    {buttons.map((num, index) => {
                        return (
                            <button
                                disabled={isDisabled}
                                key={index} value={num}
                                onClick={(e) => changeNum(e)}
                            >
                                {num}
                            </button>
                        )
                    })}
                    <button onClick={deleteNumber}>⟨</button>
                    <button onClick={checkPass}>E</button>
                </div>
            </div>
        </div>
    );
};

export default Password;